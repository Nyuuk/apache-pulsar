# How to install

```bash
git clone https://gitlab.com/Nyuuk/apache-pulsar.git
cd apache-pulsar
docker compose up -d
```

| Container      | Port Public         |
| -------------- | ------------------- |
| pulsar         | 6650:6650 8080:8080 |
| pulsar-express | 3000                |

## Setting dashboard pulsar express

![1714010525851](image/README/1714010525851.png)
